package pl.sdacademy.activityshowtime;


import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.fabric.sdk.android.Fabric;

public class SdaActivity extends AppCompatActivity implements NetworkReceiver.NetworkStateChangeListener {

    public static final String TAG = "logger tag";
    private TextView dateTimeView;
    private Button internetCheckButton;
    private SwipeRefreshLayout swipeRefreshLayout;
    private NetworkReceiver receiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_sda);

        dateTimeView = (TextView) findViewById(R.id.date_time);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.layout_swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isInternetConnectionAvailable()) {
                    displayNoInternetConnectionInfo();
                    swipeRefreshLayout.setRefreshing(true);
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        internetCheckButton = (Button) findViewById(R.id.button_internet_check);
        internetCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetConnectionAvailable()) {
                    setInternetButtonVisibility(false);
                } else {
                    setInternetButtonVisibility(true);
                }
            }
        });

        if (BuildConfig.FLAVOR.equals("demo")) {

            TextView welcomeText = (TextView) findViewById(R.id.welcome_text);
            welcomeText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SdaActivity.this, SpecialEffect.class);
                    startActivity(intent);
                }
            });
        }

        registerNetworkReceiver();
    }

    private void registerNetworkReceiver() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkReceiver(this);
        this.registerReceiver(receiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String formattedDate = getFormattedCurrentDate();
        dateTimeView.setText(formattedDate);

        if (!isInternetConnectionAvailable()) {
            displayNoInternetConnectionInfo();
            setInternetButtonVisibility(true);
        } else {
            setInternetButtonVisibility(false);
        }
    }

    private void displayNoInternetConnectionInfo() {
        LogLogger.log(TAG, "no internet connection.");
        Toast.makeText(SdaActivity.this, "no internet connection.", Toast.LENGTH_LONG).show();
    }

    private boolean isInternetConnectionAvailable() {
        return NetworkAvailabilityUtil.isInternetConnectionAvailable(SdaActivity.this);
    }

    private void setInternetButtonVisibility(boolean isVisible) {
        internetCheckButton.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    private String getFormattedCurrentDate() {
        DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance();
        Calendar calendar = Calendar.getInstance();
        LogLogger.log(TAG, "time test message: " + dateFormat.format(calendar.getTime()));
        return dateFormat.format(calendar.getTime());
    }

    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }

    public void forceCrash2(View view) {
        throw new RuntimeException("This is a crash2");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public void onNetworkStateChanged(boolean isConnected) {
        if (!isConnected) {
            displayNoInternetConnectionInfo();
            setInternetButtonVisibility(true);
            swipeRefreshLayout.setRefreshing(true);
        } else {
            setInternetButtonVisibility(false);
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
