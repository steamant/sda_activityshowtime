package pl.sdacademy.activityshowtime;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by steamant on 04.02.17.
 */

public class NetworkReceiver extends BroadcastReceiver{

    private final NetworkStateChangeListener listener;

    public NetworkReceiver(NetworkStateChangeListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean isConnectionAvailable = NetworkAvailabilityUtil.isInternetConnectionAvailable(context);
        LogLogger.log(SdaActivity.TAG, String.format(this.getClass().getSimpleName()+": isConnection: %s", isConnectionAvailable));
        listener.onNetworkStateChanged(isConnectionAvailable);
    }

    public interface NetworkStateChangeListener{

        void onNetworkStateChanged(boolean isConnected);

    }
}
