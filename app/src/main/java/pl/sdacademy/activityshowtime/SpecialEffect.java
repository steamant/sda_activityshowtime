package pl.sdacademy.activityshowtime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by steamant on 02.02.17.
 */
public class SpecialEffect extends AppCompatActivity {


    private TextView dateTimeView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sda);

        Button forceCrashButton = (Button) findViewById(R.id.button_force_crash);
        forceCrashButton.setVisibility(View.GONE);

        Button forceCrashButton2 = (Button) findViewById(R.id.button_force_crash2);
        forceCrashButton2.setVisibility(View.GONE);

        Button internetCheckButton = (Button) findViewById(R.id.button_internet_check);
        internetCheckButton.setVisibility(View.GONE);

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.layout_swipe_refresh);
        swipeRefreshLayout.setEnabled(false);

        dateTimeView = (TextView) findViewById(R.id.date_time);

        TextView welcomeText = (TextView) findViewById(R.id.welcome_text);
        welcomeText.setText(R.string.onClickText);
        welcomeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String formattedDate = getFormattedCurrentDate();
        dateTimeView.setText(formattedDate);
    }

    private String getFormattedCurrentDate() {
        DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance();
        Calendar calendar = Calendar.getInstance();
        LogLogger.log(SdaActivity.TAG, "time test message: " + dateFormat.format(calendar.getTime()));
        return dateFormat.format(calendar.getTime());
    }

    }

