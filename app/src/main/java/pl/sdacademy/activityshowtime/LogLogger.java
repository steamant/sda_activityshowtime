package pl.sdacademy.activityshowtime;

import android.util.Log;

/**
 * Created by steamant on 01.02.17.
 */

class LogLogger {

    public static void log(String tag, String message){

        if(BuildConfig.DEBUG){
            Log.d(tag,message);

        }
    }
}
